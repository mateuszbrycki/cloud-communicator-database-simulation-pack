# README #

## Wstęp ##
Aplikacja napisana na potrzeby wygenerowania dużej ilości danych do celów testowych.

Pliki z folderu sources/ pochodzą z serwisu [https://www.gutenberg.org/](Link URL).

## Dane techniczne ##
Aplikacja jest napisana w języku Java. Program generuje dane dla 12 000 wiadomości, 10 użytkowników i 10 folderów dla każdego użytkownika.