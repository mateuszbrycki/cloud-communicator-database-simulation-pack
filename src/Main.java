import sun.plugin2.message.Message;

import java.io.*;
import java.util.Random;

public class Main {



    public static void main(String[] args) throws IOException {

        MessagesGenerator messagesGenerator = new MessagesGenerator();
        Integer firstSource = messagesGenerator.run("sources/messages-pan-tadeusz.txt");
        Integer secondSource = messagesGenerator.run("sources/messages-romeo-i-julia.txt");
        Integer thirdSource = messagesGenerator.run("sources/messages-tale-of-two-cities.txt");

        Integer messagesCounter = firstSource + secondSource + thirdSource;
        System.out.println("Messages generated: " + messagesCounter);
        FolderGenerator folderGenerator = new FolderGenerator();
        folderGenerator.run();

        RelationsGenerator relationsGenerator = new RelationsGenerator();
        relationsGenerator.run(messagesCounter);

    }
}

class RelationsGenerator {

    public static String OUTPUT_FILE_NAME = "relations-sql.txt";

    public RelationsGenerator() {

    }

    public void run(Integer messagesCounter) throws IOException {

        Random generator = new Random();

        for(Integer i = 1; i <= messagesCounter; i++) {
            Integer receiversNumber = generator.nextInt(9) + 1;

            for(Integer x = 1; x <= receiversNumber; x++) {
                String messageReceiverLine = "INSERT INTO message_receiver(fk_message_id, fk_user_id, is_read) VALUES (";
                messageReceiverLine += i + ", " + x + ", false";
                messageReceiverLine += ");";

                this.saveLineToFile(messageReceiverLine);

                String messageUserFolder = "INSERT INTO user_message_folder(fk_user_id, fk_message_id, fk_folder_id) VALUES (";
                messageUserFolder += x + ", " + i + ", " + ((x * 10) + generator.nextInt(9) + 1);
                messageUserFolder += ");";

                this.saveLineToFile(messageUserFolder);
            }

        }

    }

    private Integer generateUserId() {

        Random generator = new Random();

        return generator.nextInt(10) + 1;
    }

    private void saveLineToFile(String line) throws IOException{
        PrintWriter bufferedWriter = new PrintWriter(new BufferedWriter(new FileWriter(RelationsGenerator.OUTPUT_FILE_NAME, true)));
        bufferedWriter.println(line);
        bufferedWriter.close();
    }

}

class FolderGenerator {

    public static String OUTPUT_FILE_NAME = "folders-sql.txt";

    public FolderGenerator() {

    }

    public void run() throws IOException {

        //ten users, everyone has 10 folders
        for(Integer i = 1; i <= 10; i++) {
            for(Integer y = 1; y <= 10; y++) {

                Boolean isDefaultFolder = false;

                if(y == 1) {
                    isDefaultFolder = true;
                }

                String generatedLine = "INSERT INTO folder(name, fk_owner_id, is_default_user_folder, description) VALUES (";
                generatedLine += "'Folder " + y + "', " + i + ", " + isDefaultFolder + ", 'Default description'";
                generatedLine += ");";

                this.saveLineToFile(generatedLine);
            }
        }
    }


    private void saveLineToFile(String line) throws IOException{
        PrintWriter bufferedWriter = new PrintWriter(new BufferedWriter(new FileWriter(FolderGenerator.OUTPUT_FILE_NAME, true)));
        bufferedWriter.println(line);
        bufferedWriter.close();
    }

}

class MessagesGenerator {

    public static String SOURCE_FILE_NAME = "sources/messages-source.txt";
    public static String OUTPUT_FILE_NAME = "messages-sql.txt";

    private String sourceFile;

    public MessagesGenerator() {

    }

    public Integer run(String file) throws IOException{

        if(file.isEmpty()) {
            this.sourceFile = MessagesGenerator.SOURCE_FILE_NAME;
        } else {
            this.sourceFile = file;
        }

        File answersFile = new File(this.sourceFile);

        BufferedReader bufferedReader = new BufferedReader(new FileReader(answersFile));

        String line;

        Integer i = 0;
        while ((line = bufferedReader.readLine()) != null) {

            if(!line.isEmpty()) {
                i++;

                Integer authorId = this.generateAuthorId();
                String topic = this.unifyData(line);

                String text;
                text = this.unifyData(bufferedReader.readLine());

                if(text.isEmpty()) {
                    text = "Default message text.";
                }

                String generatedLine = "INSERT INTO message(fk_author_id, topic, text) VALUES (";
                generatedLine += authorId + ", '" + topic + "', '" + text + "'";
                generatedLine += ");";


                this.saveLineToFile(generatedLine);
            }

        }

        return i;

    }

    private void saveLineToFile(String line) throws IOException{
        PrintWriter bufferedWriter = new PrintWriter(new BufferedWriter(new FileWriter(MessagesGenerator.OUTPUT_FILE_NAME, true)));
        bufferedWriter.println(line);
        bufferedWriter.close();
    }

    private Integer generateAuthorId() {

        Random generator = new Random();

        return generator.nextInt(10) + 1;
    }

    private String unifyData(String line) {
        if(line == null) {
            return "";
        }

       return line.trim().replace("'", "");
    }
}
